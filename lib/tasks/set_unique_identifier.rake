# frozen_string_literal: true

namespace 'unique_identifier' do
  desc 'It sets the Unique Identifier for Category'

  task category: :environment do
    Category.where(g_identifier: nil).find_each do |category|
      category.set_g_identifier_token
      category.save
    end
  end

  desc 'It sets the Unique Identifier for Product'

  task product: :environment do
    Product.where(p_identifier: nil).find_each do |product|
      product.set_p_identifier_token
      product.save
    end
  end
end
