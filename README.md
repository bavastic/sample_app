[![Build Status](https://travis-ci.org/atroppmann/shoppu.svg?branch=master)](https://travis-ci.org/atroppmann/shoppu)

# DESCRIPTION

This project is a basic CRUD application based on a Ruby on Rails backend that delivers data as JSON via REST.
The frontend is built on React JS and Semantic UI.

# REQUIREMENTS

- Ruby 2.4.4
- TypeScript 3.1.3
- Postgresql 9.6
- React 16.5.2
- Semantic UI React 0.83.0
- MobX 5.5.1

# SYSTEM ARCHITECTURE

- Provides JS console logger service during development
- Implements a generic store inspired by Rails.
- Distinguishes between UI and domain stores.
- Uses `axios` as transport layer to consume Rails REST API.
- Implements a simple own routing service to switch between model modules
- Uses higher order components to push domain data from store into UI components.
- UI components are stateless and have no access to stores.
- Uses modal dialogs for CRUD features
- Built on Semantic UI React.
- Provides notification service to show basic flash messages from Rails backend.

# LIVE DEMO

Visit https://xbav.herokuapp.com to see live demo. As this is a free Heroku account please be patient during spin-up of the virtual server.

# API DOCS

Visit https://xbav.herokuapp.com/swagger.html to see REST API documentation build with Swagger.

# INSTALLATION

First of all you need a Postgres database, see `database.yml` for setting up user and password.

To get the project up and ready for running follow these command line statements after checkout from Git from the project root directory.
For `development` and `production` environments there will be records created using the Faker gem. Development database population doesn't
need much time, but be patient for production seeds. See `db/seeds.rb` for details

```
bundle install
bundle exec rails webpacker:install
bundle exec rails db:setup
```

To build a production database you can use:

```
export RAILS_ENV=production
bundle exec rails db:setup
```

To run the tests just enter this command:

```
rake test
```

For development you need to run the backend and frontend services:

```
rails s
yarn run assets-server # or bin/webpack-dev-server --hot --host 127.0.0.1
```

For production mode use:

```
RAILS_ENV=production
bundle exec rails assets:precompile
rails s
```

Open your browser and visit `http://localhost:3000/pages/index` to see the Semantic UI frontend.

# Has Unique Identifier

This project uses a local gem called `has_unique_identifier`, which you can find in `lib`
(please refer to its README in order to know more about it). Basically this is used for adding one unique
identifier for `Category` and another one for `Product`, according to the requirements
specified for this technical task :). There are two `rake` tasks (`lib/tasks/set_unique_identifier.rake`)
that were defined in order to set the corresponding `identifiers` to the data that is already
in the DB but that does not have the fields populated just yet. The gem will do the job
`automagically` after creating either Product or Category, but as I assume you could
already have some data, then after setting up the local ENV you should run both tasks
in order to have all your current records with the corresponding `identifier` set correctly.

```
rake unique_identifier:category
rake unique_identifier:product
```

# What is (not) Covered on this Technical Assignment

Well, as the task said it was for up to 8 hours, I did what I could, there are still many things missing
in order to get to a very well refactored app. Basically I touched barely nothing Products, but I focused
on Categories (refactors, tests). Front-end was almost nothing, basically just adding the Unique identifier
in both the Index and the Show, as requested in the task. But I think according to the changes I did,
you can infer what is next, in terms of coding style, and focus from my side (as a developer).

I do normally squash the commits, but this time I wanted to let them there so you can see more or less
the progress.

# API access from Front-End

Please refer to the file `app/javascript/packs/stores/rest.server.ts` in order to change the URL
to `localhost` or any other where the API is hosted.

# Tests

Of course, there are some tests (Minitest :), you can find some fixtures and tests for both Models,
and some of the ones for `CategoriesController`

```
bundle exec rails test
```
