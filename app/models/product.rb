# == Schema Information
#
# Table name: products
#
#  id               :uuid             not null, primary key
#  category_id      :uuid
#  name             :string
#  price            :decimal(, )
#  currency         :string           default("EUR")
#  display_currency :string           default("EUR")
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  p_identifier     :string
#

class Product < ApplicationRecord
  include Swagger::Blocks

  default_scope -> { order("created_at ASC") }

  swagger_schema :ProductJSON do
    key :required, [:id, :name]

    property :id do
      key :type, :uuid
    end

    property :name do
      key :type, :string
      key :required, true
    end

    property :displayName do
      key :type, :string
      key :description, 'virtual field, name for displaying a record in the UI'
    end

    property :categoryId do
      key :type, :uuid
      key :description, 'reference to the ID of the category'
    end

    property :currency do
      key :type, :string
      key :description, 'currency (default: EUR)'
    end

    property :displayCurrency do
      key :type, :string
      key :description, 'currency (default: EUR)'
    end

    property :price do
      key :type, :decimal
    end

    property :uniqueIdentifier do
      key :type, :string
      key :description, 'unique identifier in the format CCC/CCC'
    end
  end

  swagger_schema :ProductCreate do
    property :categoryId do
      key :type, :uuid
    end

    property :name do
      key :type, :string
    end

    property :price do
      key :type, :decimal
    end

    property :currency do
      key :type, :string
    end

    property :displayCurrency do
      key :type, :string
    end
  end

  swagger_schema :ProductUpdate do
    property :id do
      key :type, :uuid
    end

    property :categoryId do
      key :type, :uuid
    end

    property :name do
      key :type, :string
    end

    property :price do
      key :type, :decimal
    end

    property :currency do
      key :type, :string
    end

    property :displayCurrency do
      key :type, :string
    end
  end

  belongs_to :category, counter_cache: true
  # format for p_identifier: CCC/CCC
  has_unique_identifier :p_identifier, segment_count: 2, segment_size: 3, delimiter: '/'

  validates :name, presence: true

  alias_attribute :text, :name
  alias_attribute :value, :id

  alias_attribute :displayName, :name
  alias_attribute :categoryId, :category_id
  alias_attribute :categoryName, :category_name
  alias_attribute :displayCurrency, :display_currency
  alias_attribute :uniqueIdentifier, :p_identifier

  delegate :name, to: :category, prefix: true

  def as_json(options = {})
    super(
      options.keys.any? ? options : {
        only: [:id, :name, :currency, :price],
        methods: [:displayName, :categoryId, :categoryName, :displayCurrency, :uniqueIdentifier],
      }
    )
  end
end
