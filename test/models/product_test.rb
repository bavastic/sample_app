# == Schema Information
#
# Table name: products
#
#  id               :uuid             not null, primary key
#  category_id      :uuid
#  name             :string
#  price            :decimal(, )
#  currency         :string           default("EUR")
#  display_currency :string           default("EUR")
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  p_identifier     :string
#

require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  test 'creates Products with the correct params' do
    assert_equal products(:one).category, categories(:two)
    assert_equal products(:one).p_identifier, 'AAA/BBB'
    assert_equal products(:one).name, 'Product One'
    assert_equal products(:one).price, 10.0
    assert_equal products(:one).currency, 'EUR'
    assert_equal products(:one).display_currency, 'EUR'
  end

  test 'it does not create a Product with missing values' do
    name = 'Prod'

    assert_no_changes 'Product.count' do
      Product.create(name: name)
    end
  end

  test 'it creates a Product with the correct default values' do
    product = Product.create(name: 'Prod', category: categories(:two))

    assert_equal product.category, categories(:two)
    assert_equal product.name, 'Prod'
    assert_equal product.currency, 'EUR'
    assert_equal product.display_currency, 'EUR'
    assert_match(/\D{3}\/\D{3}/, product.p_identifier)
  end

  test 'it creates a Product with the correct values for currency' do
    product = Product.create(name: 'Prod', category: categories(:two), currency: 'USD', display_currency: 'USD')

    assert_equal product.currency, 'USD'
    assert_equal product.display_currency, 'USD'
  end

  test 'it changes the product_count when creating a new Product' do
    assert_changes "Category.find_by(g_identifier: 'AA-BB-CD').products_count" do
      Product.create(name: 'Prod', category: categories(:two))
    end
  end

  test 'it changes the product_count when deleting a Product' do
    assert_changes "Category.find_by(g_identifier: 'AA-BB-CD').products_count" do
      products(:two).destroy
    end
  end
end
