# == Schema Information
#
# Table name: categories
#
#  id             :uuid             not null, primary key
#  parent_id      :uuid
#  name           :string
#  products_count :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  g_identifier   :string
#

require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test 'creates Categories with the correct params' do
    assert_nil categories(:one).parent
    assert_equal categories(:one).g_identifier, 'AA-BB-CC'

    assert_equal categories(:two).products.count, 2
    assert_equal categories(:two).parent, categories(:one)

    assert_equal categories(:three).products.count, 1
    assert_equal categories(:three).parent, categories(:two)
  end

  test 'it does not create a Category with the a duplicated name' do
    name = 'Cat'

    assert_changes 'Category.count' do
      Category.create(name: name)
    end

    assert_raise ActiveRecord::RecordNotUnique do
      Category.create(name: name)
    end
  end

  test 'it creates a Category with the correct Parent' do
    category = Category.create(name: 'With Parent', parent: categories(:one))

    assert_equal category.parent, categories(:one)
  end

  test 'it sets the correct format for g_identifier' do
    category = Category.create(name: 'Name')

    assert_match(/\D{2}-\D{2}-\D{2}/, category.g_identifier)
  end
end
