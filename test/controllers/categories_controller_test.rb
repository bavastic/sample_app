# frozen_string_literal: true

require 'test_helper'

class CategoriesControllerTest < ActionDispatch::IntegrationTest
  test 'it fetch all the categories' do
    get categories_path

    body = JSON.parse(response.body)

    assert_response :success
    assert_equal body.size, Category.count
  end

  test 'it brings the correct data of the categories' do
    get categories_path

    first_category = JSON.parse(response.body).first.with_indifferent_access

    category = Category.find(first_category[:id])

    assert_response :success
    assert_equal first_category[:name], category.name
    assert_equal first_category[:displayName], category.name
    # Just in case it is nil for ROOT
    assert first_category.include?(:parentId)
    assert first_category.include?(:parentName)
    assert_equal first_category[:productsCount], category.products_count
    assert_equal first_category[:uniqueIdentifier], category.g_identifier
  end

  test 'it gets the correct Count for Categories' do
    get categories_count_path

    body = JSON.parse(response.body)

    assert_equal body['count'], Category.count
  end

  test 'it gets the correct Category according to its ID' do
    category = categories(:two)

    get category_path(category)

    body = JSON.parse(response.body).with_indifferent_access

    assert_equal body[:id], category.id
    assert_equal body[:name], category.name
    assert_equal body[:displayName], "ROOT > #{category.name}"
    assert_equal body[:parentId], category.parent.id
    assert_equal body[:parentName], category.parent.name
    assert_equal body[:productsCount], category.products_count
    assert_equal body[:uniqueIdentifier], category.g_identifier
  end

  test 'it raises record not found for show' do
    get category_path(-1)

    assert_response :not_found

    assert response.body.include?('record not found')
  end

  test 'it deletes the Category' do
    assert_difference -> { Category.count } => -1 do
      delete category_path(categories(:two))
    end
  end

  test 'it raises record not found for delete' do
    delete category_path(-1)

    assert_response :not_found

    assert response.body.include?('record not found')
  end

  test 'it updates the Category' do
    category = categories(:two)
    new_name = 'New Name'
    params = { id: category, name: new_name }

    put category_path(category, params: { category: params })

    category.reload

    assert_equal category.name, new_name
  end

  test 'it gets the category options' do
    get categories_options_path

    body = JSON.parse(response.body)

    assert_equal body.size, Category.count

    first_category = body.first

    assert_equal first_category.keys, ["value", "text"]
  end
end
