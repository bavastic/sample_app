class AddGIdentifierToCategory < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :g_identifier, :string
    add_index :categories, :g_identifier
  end
end
