class AddGIdentifierToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :p_identifier, :string
    add_index :products, :p_identifier
  end
end
